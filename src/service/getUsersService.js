import promisePool from '../utils/dbConnection'

export async function getUsers (event, context) {
  const SQLQuery = 'SELECT * FROM users'
  const data = await promisePool.query(SQLQuery)
  return data[0]
}
