import aws from 'aws-sdk'
import config from '../../env.json'

const awsConfig = {
  accessKeyId: config.Parameters.accessKey,
  secretAccessKey: config.Parameters.secretAccessKey
}

aws.config.update(awsConfig)

const s3 = new aws.S3()

export async function s3Upload (fileName, fileType) {
  const key = fileName
  const trimmedKey = key.replace(/[`~ !@#$%^&*()|+=?;:'",<>{}[\]\\]/gi, '')
  const expires = 60000
  let ContentType
  if (fileType === 'jpg' || fileType === 'jpeg') {
    ContentType = 'image/jpeg'
  }
  if (fileType === 'png') {
    ContentType = 'image/png'
  }
  const signedUrl = await s3.getSignedUrl('putObject', {
    Bucket: config.Parameters.s3Bucket,
    Key: trimmedKey,
    Expires: expires,
    ContentType: ContentType
  })
  const imageDetail = {
    url: signedUrl,
    expires,
    uploadUrl: signedUrl,
    accessUrl: `${config.Parameters.s3Bucket}/${trimmedKey}`
  }
  return imageDetail
}

export async function s3Download (file) {
  console.log(file)
  console.log(config.Parameters.s3Bucket)
  const url = s3.getSignedUrl('getObject', {
    Bucket: config.Parameters.s3Bucket,
    Key: file,
    Expires: 60000
  })

  return {
    url: url
  }
}
