/* eslint-disable camelcase */
import AWS from 'aws-sdk'
import config from '../../env.json'

AWS.config.update({
  accessKeyId: config.Parameters.accessKeyId,
  secretAccessKey: config.Parameters.secretAccessKey,
  region: config.Parameters.region
})

const ses = new AWS.SES({ apiVersion: '2010-12-01' })

export async function email (to, subject, message) {
  const params = {
    Destination: {
      ToAddresses: [to]
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: emailBody(to)
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    },
    Source: 'sneha.herle@fibonalabs.com'
  }

  await ses.sendEmail(params).promise().then(console.log('Finished sending email'))
}

export function emailBody (username) {
  return `
  Hi ${username},<br/><br/>
  This is sample email sent using AWS SES service!
  <br/><br/>
  `
}
