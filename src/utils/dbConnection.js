import mysql from 'mysql2/promise'
import config from '../../env.json'

const pool = mysql.createPool({
  host: config.Parameters.databaseHost,
  user: config.Parameters.databaseUserName,
  password: config.Parameters.databasePass,
  database: config.Parameters.databaseName
})

const promisePool = pool

console.log('database successfully connected')

export default promisePool
