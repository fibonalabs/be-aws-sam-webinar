export const SUCCESS = 200
export const NOT_FOUND = 404
export const SERVER_ERROR = 500
export const SUCCESS201 = 201
export const BAD_REQUEST = 400
