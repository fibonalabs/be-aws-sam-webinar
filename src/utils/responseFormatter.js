import { SUCCESS, SERVER_ERROR, NOT_FOUND, BAD_REQUEST, SUCCESS201 } from '../utils/statusCode'

export const formatResponseSuccess = (body, headers = {}) => {
  const response = {
    statusCode: SUCCESS,

    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'OPTIONS,POST,GET,DELETE,PUT,PATCH',
      'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
      'Access-Control-Expose-Headers': 'Date, x-api-id',
      'X-Requested-With': '*',
      ...headers
    },

    body: JSON.stringify({
      body
    })

  }

  return response
}

export const formatJSONResponseError = (err, headers = {}) => {
  console.log(err)

  const response = {
    statusCode: SERVER_ERROR,

    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      ...headers
    },

    body: JSON.stringify({
      msg: 'There was an internal server error. Please try again later.',
      err: err
    })

  }

  return response
}

export const formatJSONResponseNotFound = (err, headers = {}) => {
  const response = {

    statusCode: NOT_FOUND,

    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'OPTIONS,POST,GET,DELETE,PUT,PATCH',
      'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
      'Access-Control-Expose-Headers': 'Date, x-api-id',
      'X-Requested-With': '*',
      ...headers
    },

    body: JSON.stringify({
      msg: 'Resource Not Found',
      err: err
    })

  }

  return response
}

export const formatResponseSuccess201 = (body, headers = {}) => {
  const response = {
    statusCode: SUCCESS201,

    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'OPTIONS,POST,GET,DELETE,PUT,PATCH',
      'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
      'Access-Control-Expose-Headers': 'Date, x-api-id',
      'X-Requested-With': '*',
      ...headers
    },

    body: JSON.stringify({
      body
    })

  }

  return response
}

export const formatJSONResponseBadRequest = (err, headers = {}) => {
  const response = {

    statusCode: BAD_REQUEST,

    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'OPTIONS,POST,GET,DELETE,PUT,PATCH',
      'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
      'Access-Control-Expose-Headers': 'Date, x-api-id',
      'X-Requested-With': '*',
      ...headers
    },

    body: JSON.stringify({
      msg: 'Request Not Formatted Properly',
      err: err
    })

  }

  return response
}
