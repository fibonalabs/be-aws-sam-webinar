import { formatResponseSuccess, formatJSONResponseError } from '../utils/responseFormatter'
import { email } from '../utils/ses'

export async function sendSESEmailFunction (event, context) {
  try {
    const body = JSON.parse(event.body)
    const username = body.username
    await email(username, 'SES Demo')
    return formatResponseSuccess({ msg: 'Finished sending email' })
  } catch (err) {
    return formatJSONResponseError(err)
  }
}
