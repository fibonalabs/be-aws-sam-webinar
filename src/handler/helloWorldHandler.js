
import { formatResponseSuccess } from '../utils/responseFormatter'

export async function helloWorld (event, context) {
  try {
    console.log('Hello')
    return formatResponseSuccess('hello')
  } catch (err) {
    return formatResponseSuccess(err)
  }
}
