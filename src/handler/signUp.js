import { formatResponseSuccess, formatJSONResponseError } from '../utils/responseFormatter'
const config = require('../../env.json')
const Cognito = require('amazon-cognito-identity-js')
const userPool = new Cognito.CognitoUserPool({
  UserPoolId: config.Parameters.UserPoolId,
  ClientId: config.Parameters.ClientId
})
let CognitoUserCreated = 0

const signUpUser = (username, password) =>
  new Promise((resolve, reject) => {
    const attributeList = []
    const dataEmail = {
      Name: 'email',
      Value: username
    }
    const dataPhoneNumber = {
      Name: 'phone_number',
      Value: '+919425009780' // your phone number here with +country code and no delimiters in front
    }
    const attributeEmail = new Cognito.CognitoUserAttribute(dataEmail)
    const attributePhoneNumber = new Cognito.CognitoUserAttribute(dataPhoneNumber)
    attributeList.push(attributeEmail)
    attributeList.push(attributePhoneNumber)
    userPool.signUp(username, password, attributeList, null, (error, result) => {
      if (error) { reject(error) } else {
        console.log(result.userSub)
        resolve(result)
      }
    })
  }
  )

const signUp = async (username, password) => {
  try {
    const response = await signUpUser(username, password)
    CognitoUserCreated = 1
    return response.userSub
  } catch (e) {
    console.log(e)
    return e
  }
}

export async function signUpFunctionFunction (event, context) {
  try {
    const data = JSON.parse(event.body)
    const response = await signUp(data.email, data.password)
    console.log('Hello')
    if (CognitoUserCreated) {
      return formatResponseSuccess({ msg: 'User Successfully Created and Confirmed in Cognito' })
    }
    return formatJSONResponseError({ msg: response.message })
  } catch (err) {
    return formatJSONResponseError(err)
  }
}
