/* eslint-disable camelcase */
import promisePool from '../utils/dbConnection'

export async function postConfirmationTriggerFunction (event, context) {
  console.log(event)
  console.log(event.userName)
  if (event.request.userAttributes.email && event.request.userAttributes.phone_number) {
    const SQLQuery = `INSERT INTO users(aws_cognito_sub_id, user_email, user_phone_num) 
                        values ('${event.request.userAttributes.sub}',
                        '${event.request.userAttributes.email}','${event.request.userAttributes.phone_number}')`

    const [res] = await promisePool.query(SQLQuery)
    console.log(res)
    // Return to Amazon Cognito
    context.done(null, event)
  } else {
    // Nothing to do, the user's email ID is unknown
    context.done(null, event)
  }
};
