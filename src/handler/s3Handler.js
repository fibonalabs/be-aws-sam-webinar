import { formatResponseSuccess, formatJSONResponseError } from '../utils/responseFormatter'
import { s3Upload, s3Download } from '../utils/s3'

export async function s3UploadFunction (event, context) {
  try {
    const body = JSON.parse(event.body)
    const fileName = body.file_name
    const fileType = body.file_type
    const response = await s3Upload(fileName, fileType)
    return formatResponseSuccess(response)
  } catch (err) {
    return formatJSONResponseError(err)
  }
}

export async function s3DownloadFunction (event, context) {
  try {
    const body = JSON.parse(event.body)
    const fileName = body.file_name
    const response = await s3Download(fileName)
    return formatResponseSuccess(response)
  } catch (err) {
    return formatJSONResponseError(err)
  }
}
