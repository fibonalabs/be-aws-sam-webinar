import { getUsers } from '../service/getUsersService'
import { formatResponseSuccess, formatJSONResponseError } from '../utils/responseFormatter'

export async function getUsersFunction (event, context) {
  try {
    console.log('Hello')
    console.log('change for deployment')
    const response = await getUsers()
    return formatResponseSuccess(response)
  } catch (err) {
    return formatJSONResponseError(err)
  }
}
