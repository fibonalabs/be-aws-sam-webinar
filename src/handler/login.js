import { formatResponseSuccess, formatJSONResponseError } from '../utils/responseFormatter'
const config = require('../../env.json')
const AWS = require('aws-sdk')

const login = async (username, password) => {
  try {
    const cognito = new AWS.CognitoIdentityServiceProvider()
    const response = await cognito.adminInitiateAuth({
      UserPoolId: config.Parameters.UserPoolId,
      ClientId: config.Parameters.ClientId,
      AuthFlow: 'ADMIN_NO_SRP_AUTH',
      AuthParameters: {
        USERNAME: username,
        PASSWORD: password
      }
    }).promise()
    return response
  } catch (err) {
    return err
  }
}

export async function lambdaHandler (event, _context) {
  const body = JSON.parse(event.body)
  return userLogin(body)
}

const userLogin = async ({ username, password }) => {
  try {
    const response = await login(username, password)
    return formatResponseSuccess({
      msg: 'User Successfully Logged in!',
      idToken: response.AuthenticationResult.IdToken,
      AccessToken: response.AuthenticationResult.AccessToken
    })
  } catch (e) {
    console.log(e)
    return formatJSONResponseError({ msg: e.message })
  }
}
