const path = require('path')

module.exports = {
  entry: {
    helloWorldHandler: './src/handler/helloWorldHandler.js',
    getUsersHandler: './src/handler/getUsersHandler.js',
    postConfirmationTrigger: './src/handler/postConfirmationTrigger.js',
    preSignUpTrigger: './src/handler/preSignUpTrigger',
    signUp: './src/handler/signUp.js',
    login: './src/handler/login.js',
    sendSESEmail: './src/handler/sendSESEmail.js',
    s3Handler: './src/handler/s3Handler.js'
  },
  output: {
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
    sourceMapFilename: '[file].map'
  },
  mode: process.env.NODE_ENV === 'prod' ? 'production' : 'development',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: ['babel-loader'],
        include: [path.join(__dirname, 'src')]
      },
      {
        test: /\.html$/i,
        use: 'raw-loader'
      },
      {
        test: /\.ejs$/,
        use: 'ejs-compiled-loader'
      }
    ]
  },
  externals: ['aws-sdk', 'commonjs2 firebase-admin', 'commonjs @google-cloud/storage'],
  target: 'node',
  optimization: {
    minimize: false
  },
  performance: {
    hints: false
  }

}
