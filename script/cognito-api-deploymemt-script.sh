S3_BUCKET=aws-webinar-s3-bucket
INPUT_FILE=template.yaml
OUTPUT_FILE=sam.yaml
STAGE_NAME=dev
STACK_NAME=aws-webinar-$STAGE_NAME
# Copy Swagger file to S3 to be able to transform its content
# for "deploying" it to API Gateway

# Package the application
aws cloudformation package --template-file $INPUT_FILE --output-template-file $OUTPUT_FILE --s3-bucket $S3_BUCKET

# Deploy the application
aws cloudformation deploy --template-file $OUTPUT_FILE --stack-name $STACK_NAME --parameter-overrides userIamRool='XXXXXXXXXXXXXXXXXX' cognitoUserPoolArn='XXXXXXXXXXXXXXXXXXX' StageName=$STAGE_NAME S3BucketName=$S3_BUCKET --capabilities CAPABILITY_IAM
